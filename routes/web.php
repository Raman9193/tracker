<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\emailController;


Route::get('/', function () {
    return view('welcome');
});



Route::get('/list', 'App\Http\Controllers\emailController@listAllEmails')->name('emails.list');

Route::get('/send', 'App\Http\Controllers\emailController@send')->name('emails.send');
Route::get('/reset', 'App\Http\Controllers\emailController@reset')->name('emails.reset');

Route::get('seen/{id}','App\Http\Controllers\emailController@seen')->name('emails.seen');



