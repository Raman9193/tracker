<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\Models\email;
use Redirect;
use DB;

class emailController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function listAllEmails(){
        
        $emails = email::latest()->paginate(10);
        $noEmailSent= email::all()->count();   

        $noEmailsViewed=DB::table('emails')->where('viewed', 1)->count();
   
        return view('emails.list',compact('emails','noEmailSent','noEmailsViewed'));
    }
    public function reset(){
        $dataToReset=email::all();
         foreach ($dataToReset as $reset) {
            $reset->viewed=0 ;
            $reset->save();
         }
         return redirect()->route('emails.list');
        }


    public function Send()
    {
        $data=email::all();
      
        
     foreach ($data as $rec) {
        Mail::to($rec->email)->send(new WelcomeMail($rec)); 
     }
     return redirect()->route('emails.list');

    }

   public function seen($id){
    
        $seen=email::findOrFail($id);
      
        if($seen->viewed==0)
        {
            $seen->viewed=1;
            $seen->save();
        }

    }
    
    
}
