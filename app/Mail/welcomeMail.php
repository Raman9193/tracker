<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\email;

class welcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    private $rec;
    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($rec)
    {
        $this->rec = $rec;

        return $this->rec;
      
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            return $this->markdown('emails.welcome')
            ->with(['emailid' => $this->rec->id]);
       
        }
    
}
