<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email Tracker</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    
    <link href="{{ asset('css/dashCards.css') }}" rel="stylesheet">





</head>
<body style="margin: 20px 20px 20px 20px">
    {{-- stats --}}
    <div class="meAdd">
        <div class="container">

            <div class="row">
            <div class="col-md-3">
              <div class="card-counter primary">
                <i class="fa fa-code-fork"></i>
                <span class="count-numbers">{{$noEmailSent}} </span>
                <span class="count-name">Emails Sent</span>
              </div>
            </div>
        
            <div class="col-md-3">
              <div class="card-counter danger">
                <i class="fa fa-ticket"></i>
                <span class="count-numbers">{{$noEmailsViewed}}</span>
                <span class="count-name">Emails viewed</span>
              </div>
            </div>
            </div>
        </div>

             

    </div>
    {{-- table for emials --}}
    <div>
        <label for="title">Email Open Tracker</label>
    <table class="table table-striped table-dark">
        <thead>
          <tr>
            <th scope="col">id</th>
            <th scope="col">Email</th>
            <th scope="col">Viewed</th>
          </tr>
        </thead>

          @foreach ($emails as $data)
          <tbody>
          <tr>
            <th scope="row">{{$data->id}}</th>
     
              <td>{{$data->email}}</td>
              <td>{{$data->viewed}}</td>
          </tr>
    
          @endforeach
        </tbody>

      </table>
    </div>

        <a href="{{ route('emails.send') }}" style="background-color: green;">Send </a>


        <a href="{{ route('emails.reset') }}"> Reset All </a>

  
 
    
      
        
    









<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>